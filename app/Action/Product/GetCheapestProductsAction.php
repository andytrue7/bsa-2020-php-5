<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->repository = $productRepository;
    }

    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->repository->findCheapest();

        return new GetCheapestProductsResponse($products);
    }
}
