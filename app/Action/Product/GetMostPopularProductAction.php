<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    private $repository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->repository = $productRepository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $product = $this->repository->findPopular();

        return new GetMostPopularProductResponse($product);
    }
}
