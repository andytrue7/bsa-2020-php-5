<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

class ProductRepository implements ProductRepositoryInterface
{
	private $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }

    public function findPopular(): Product
    {
        return $this->sortByPopular()[0];
    }

    public function findCheapest(): array
    {
        $cheapProducts = [];

        $sortedProducts = $this->sortByPrice();

        $count = 0;

        foreach ($sortedProducts as $product) {

            $cheapProducts[] = $product;

            $count++;

            if ($count == 3) {
                break;
            }
        }

        return $cheapProducts;
    }

    private function sortByPrice(): array
    {
        $productsCopy = $this->products;

        usort($productsCopy, function ($a, $b) {
            return $a->getPrice() - $b->getPrice();
        });

        return $productsCopy;
    }

    private function sortByPopular(): array
    {
        $productsCopy = $this->products;

        usort($productsCopy, function ($a, $b) {
            return $b->getRating() - $a->getRating();
        });

        return $productsCopy;
    }
}
