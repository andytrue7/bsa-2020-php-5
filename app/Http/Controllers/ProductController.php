<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;


class ProductController extends Controller
{
    private $getAllProductsAction;
    private $getPopularProductAction;
    private $getCheapestProductsAction;
    private $presenter;

    public function __construct(
        GetAllProductsAction $getAllProductsAction,
        GetMostPopularProductAction $getMostPopularProductAction,
        GetCheapestProductsAction $getCheapestProductsAction,
        ProductArrayPresenter $productArrayPresenter
    )
    {
        $this->getAllProductsAction = $getAllProductsAction;
        $this->getPopularProductAction = $getMostPopularProductAction;
        $this->getCheapestProductsAction = $getCheapestProductsAction;
        $this->presenter = $productArrayPresenter;
    }

    public function getAllProducts(): JsonResponse
    {
        $response = $this->getAllProductsAction->execute();

        return response()->json($this->presenter::presentCollection($response->getProducts()));
    }

    public function getPopularProduct(): JsonResponse
    {
        $response = $this->getPopularProductAction->execute();

        return response()->json($this->presenter::present($response->getProduct()));
    }

    public function getCheapProducts(): View
    {
        $response = $this->getCheapestProductsAction->execute();

        $products = $this->presenter::presentCollection($response->getProducts());

        return view('cheap_products', ['products' => $products]);
    }
}
