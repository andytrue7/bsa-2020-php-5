<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;


class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $products = [];

        for($i = 1; $i <= 10; $i++) {

            $products[] = new Product(
                $i,
                'product ' . $i,
                self::getRandom(10, 100),
                'http://lorempixel.com/400/200/food/'. rand(1, 10),
                self::getRandom(0, 10)
            );

        }

        return $products;
    }

    private static function getRandom($min, $max)
    {
        return round($min + mt_rand()/mt_getrandmax()  * ($max - $min), 2);
    }
}
