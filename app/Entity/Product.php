<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    private $id;
    private $name;
    private $price;
    private $image;
    private $rating;

    public function __construct(int $id, string $name, float $price, string $image, float $rating)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->image = $image;
        $this->rating = $rating;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getImageUrl()
    {
        return $this->image;
    }

    public function getRating()
    {
        return $this->rating;
    }
}
