<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    List of cheap products
    <ul>
    @forelse($products as $product)
        <li>
            <p>Id: {{ $product['id'] }}</p>
            <p>Name: {{ $product['name'] }}</p>
            <p>Price: {{ $product['price'] }}</p>
            <p>Image: <img src="{{ $product['img'] }}" alt=""></p>
            <p>Rating: {{ $product['rating'] }}</p>
        </li>

    @empty
        <p>No products</p>
    @endforelse
    </ul>

</body>
</html>
